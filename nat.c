//the mainframe has referred to nftest.c from CSCI4430 lecture.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netinet/ip.h>
#include <netinet/tcp.h>
#include <linux/netfilter.h>  
#include <libnetfilter_queue/libnetfilter_queue.h>
#include <arpa/inet.h>
#include "checksum.h"
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/ioctl.h>        // macro ioctl is defined
#include <bits/ioctls.h>      // defines values for argument "request" of ioctl.
#include <net/if.h>           // struct ifreq


#define PORT_OFFSET 10000
#define QUEUE_NUM 0
#define PORT_RANGE 2001

unsigned int local_mask;
unsigned int local_network;//a host byte order ip
unsigned int public_ip;//host byte order
unsigned char *pktData;//IP payload
int len;//the length of IP payload

typedef struct { 
	char status;
	unsigned int in_ip;//host byte order
	unsigned short in_port; //host byte order
} entry;
//structure of table: 1 byte 1/0 indicating whether in use; 
// 4 byte internal ip; 2 byte internal port
//the external port should be got using index+PORT_OFFSET
static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, 
		struct nfq_data *pkt, void *data);
void display_table(entry** table);
void send_new_p(int len, unsigned char* pktData,struct ip* iph,const char* eth_interface);//send modified packet in raw form
void forward_p (struct ip* iph,struct tcphdr* tcph, unsigned int new_ip, unsigned short new_port);//before it starts, iph is still pointing to old packet
void update_checksum(struct ip* iph,struct tcphdr* tcph);//update the checksum

void forward_p(struct ip* iph,struct tcphdr* tcph, unsigned int new_ip, unsigned short new_port)
{
	unsigned int tmp;
	unsigned int s_ip;
	tmp=*((unsigned int*)&(iph->ip_src));
	s_ip=ntohl(tmp);
	if((s_ip & local_mask) == local_network)//outbound
	{
		unsigned short tmp_s;
		tmp_s=htons(new_port);
		memcpy(&(tcph->source),&tmp_s,sizeof(tmp_s));
		tmp=htonl(new_ip);
		memcpy(&(iph->ip_src),&tmp,sizeof(tmp));
		update_checksum(iph,tcph);
		send_new_p(len,pktData,iph,"eth1");
	}
	else//inbound
	{
		unsigned short tmp_s;
		tmp_s=htons(new_port);
		memcpy(&(tcph->dest),&tmp_s,sizeof(tmp_s));
		tmp=htonl(new_ip);
		memcpy(&(iph->ip_dst),&tmp,sizeof(tmp));
		update_checksum(iph,tcph);
		send_new_p(len,pktData,iph,"eth0");
	}
	return;
}
void update_checksum(struct ip* iph,struct tcphdr* tcph)
{
	unsigned short tmp_checksum=0;
	tcph->check=0;
	tmp_checksum=tcp_checksum((unsigned char*)iph);
	tcph->check=tmp_checksum;

	iph->ip_sum=0;
	tmp_checksum=ip_checksum((unsigned char*)iph);
	iph->ip_sum=tmp_checksum;
}
void send_new_p(int len, unsigned char* pktData,struct ip* iph,const char* eth_interface)
{
	////begin to send the modified packet
	struct sockaddr_in sin;
	memset(&sin,0,sizeof(struct sockaddr_in));
	sin.sin_family=AF_INET;
	sin.sin_addr.s_addr=(iph->ip_dst).s_addr;

	char interface[40];
	int sd;
	const int on=1;
	struct ifreq ifr;

	strcpy (interface, eth_interface);

	// Submit request for a socket descriptor to look up interface.
	if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
		perror ("socket() failed to get socket descriptor for using ioctl() ");
		exit (EXIT_FAILURE);
	}

	// Use ioctl() to look up interface index which we will use to
	// bind socket descriptor sd to specified interface with setsockopt() since
	// none of the other arguments of sendto() specify which interface to use.
	memset (&ifr, 0, sizeof (ifr));
	snprintf (ifr.ifr_name, sizeof (ifr.ifr_name), "%s", interface);
	if (ioctl (sd, SIOCGIFINDEX, &ifr) < 0) {
		perror ("ioctl() failed to find interface ");
		exit (EXIT_FAILURE);
	}
	close (sd);
	//printf ("Index for interface %s is %i\n", interface, ifr.ifr_ifindex);

	if ((sd = socket (AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
		perror ("socket() failed ");
		exit (EXIT_FAILURE);
	}

	// Set flag so socket expects us to provide IPv4 header.
	if (setsockopt (sd, IPPROTO_IP, IP_HDRINCL, &on, sizeof (on)) < 0) {
		perror ("setsockopt() failed to set IP_HDRINCL ");
		exit (EXIT_FAILURE);
	}

	// Bind socket to interface index.
	if (setsockopt (sd, SOL_SOCKET, SO_BINDTODEVICE, &ifr, sizeof (ifr)) < 0) {
		perror ("setsockopt() failed to bind to interface ");
		exit (EXIT_FAILURE);
	}

	// Send packet.
	if (sendto (sd, pktData, len, 0, (struct sockaddr *) &sin, sizeof (struct sockaddr)) < 0)  {
		perror ("sendto() failed ");
		exit (EXIT_FAILURE);
	}
	close(sd);

	////end of sending modified packet
}

void display_table(entry** table)
{
	struct in_addr tmp1;
	struct in_addr tmp2;
	unsigned int public_ip_n;
	unsigned int inter_ip_n;
	int i;
	printf("Public ip\t\tPublic port\t\tInternal ip\t\tInternal port\n");
	for(i=0;i<PORT_RANGE;i++)
		if(table[i]->status)
		{
			public_ip_n=htonl(public_ip);
			inter_ip_n=htonl(table[i]->in_ip);
			memcpy(&tmp1,&public_ip_n,sizeof(public_ip));
			memcpy(&tmp2,&inter_ip_n,sizeof(unsigned int));
			//if the two printf below are merged, 
			//undesired result may happen due to buffer issue of inet_ntoa
			printf("%s\t\t%d",inet_ntoa(tmp1),i+PORT_OFFSET);
			printf("\t\t\t%s\t\t%d\n",inet_ntoa(tmp2),table[i]->in_port);
		}
	printf("End of the table.\n");
	return;
}
static int cb(struct nfq_q_handle *qh, struct nfgenmsg *nfmsg, 
		struct nfq_data *pkt, void *data) 
{
	entry** table=(entry**)data;
	unsigned int s_ip;
	unsigned int d_ip;
	unsigned short s_port;//host byte order
	unsigned short d_port;//host byte order
	unsigned int id;
	unsigned int tmp;
	//unsigned short tmp_s;
	struct nfqnl_msg_packet_hdr *header;

	//get id
	header = nfq_get_msg_packet_hdr(pkt);
	id=ntohl(header->packet_id);
	//get ip packet
	len=nfq_get_payload(pkt, (char**)(&pktData));
	if (len<0)
	{
		fprintf(stderr, "Error in nfq_get_payload()\n");
		exit(1);
	}
	//get source ip and destination ip
	struct ip* iph=(struct ip*)pktData;
	tmp=*((unsigned int*)&(iph->ip_src));
	s_ip=ntohl(tmp);
	//printf("s_ip is %x\n", s_ip);
	tmp=*((unsigned int*)&(iph->ip_dst));
	d_ip=ntohl(tmp);
	//printf("d_ip is %x\n", d_ip);
	//get source port and destination port
	if(iph->ip_p != IPPROTO_TCP)
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
	struct tcphdr* tcph=(struct tcphdr*)(((char*)iph)+ ((iph->ip_hl) << 2));
	s_port=ntohs(tcph->source);
	//printf("s_port is %d\n", s_port);
	d_port=ntohs(tcph->dest);
	//printf("d_port is %d\n", d_port);
	//d_port=tcph->dest;

	if(d_port==22 || s_port==22)//accept all SSH packets
		return nfq_set_verdict(qh, id, NF_ACCEPT, 0, NULL);


	if((s_ip & local_mask) == local_network)//outbound
	{
		//whether the entry exists
		int found=-1;//will become the index found in table, or remain -1 if not found
		int i;
		for(i=0;i<PORT_RANGE;i++)
		{
			if(table[i]->status)
			{
				if(table[i]->in_ip == s_ip && table[i]->in_port== s_port)
				{
					found=i;
					break;
				}
			}
		}
		if(found>-1)//found
		{

			if(tcph->ack)//is ack
			{
				//printf("%d\n",table[found]->status);
				if(table[found]->status >= 3)//last ack of fin handshake
				{
					table[found]->status=0;
					display_table(table);
				}
			}
			if(tcph->rst)//is rst
			{
				table[found]->status=0;
				display_table(table);
			}
			if(tcph->fin)//is fin, status+=1 until 3
			{
				table[found]->status+=1;
			}
			forward_p(iph,tcph, public_ip, found+PORT_OFFSET);
			return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
		}
		else
		{
			if(tcph->syn)//is syn, create new entry
			{
				int i;
				for(i=0;i<PORT_RANGE;i++)
					if(table[i]->status==0)
						break;
				if(i>2000)
					printf("The table is full.\n");
				table[i]->status=1;
				table[i]->in_ip=s_ip;
				table[i]->in_port=s_port;
				display_table(table);
				forward_p(iph,tcph,public_ip,i+PORT_OFFSET);
			}
			//if not syn, drop directly
			return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
		}
	}
	else//inbound
	{
		if(table[d_port-PORT_OFFSET]->status)//found
		{
			if(tcph->ack)//is ack
			{
				if(table[d_port-PORT_OFFSET]->status >= 3)//last ack of fin handshake
				{
					table[d_port-PORT_OFFSET]->status=0;
					display_table(table);
				}
			}
			if(tcph->rst)//is rst
			{
				table[d_port-PORT_OFFSET]->status=0;
				display_table(table);
			}
			if(tcph->fin)//is fin, status+=1 until 3
			{
				table[d_port-PORT_OFFSET]->status+=1;
			}
			forward_p(iph,tcph, table[d_port-PORT_OFFSET]->in_ip,table[d_port-PORT_OFFSET]->in_port);
		}
		//if not found, drop directly
		return nfq_set_verdict(qh, id, NF_DROP, 0, NULL);
	}
}

int main(int argc,char** argv)
{
	if(argc!=4)
	{
		printf("Usage: ./nat <public ip> <internal ip> <subnet mask>\n");
		exit(0);
	}
	int mask_int=atoi(argv[3]);
	local_mask=0xffffffff<<(32-mask_int);

	struct in_addr addr;
	inet_aton(argv[1],&addr);
	public_ip=ntohl(*((unsigned int*)&addr));
	//public_ip=ntohl(public_ip);

	unsigned int internal_ip;
	inet_aton(argv[2],&addr);
	internal_ip=ntohl(*((unsigned int*)&addr));
	//internal_ip=ntohl(internal_ip);
	local_network=internal_ip & local_mask;

	entry** table=(entry**)malloc(sizeof(entry*)*PORT_RANGE);
	int i;
	for(i=0;i<PORT_RANGE;i++)
	{
		table[i]=(entry*)malloc(sizeof(entry));
		table[i]->status=0;
	}


	//open handle
	struct nfq_handle* h=nfq_open();
	if (!h)
	{
		fprintf(stderr, "Error in nfq_open()\n");
		exit(1);
	}
	//unbind
	if (nfq_unbind_pf(h, AF_INET) < 0) 
	{
		fprintf(stderr, "Error in nfq_unbind_pf()\n");
		exit(1);
	}
	//bind
	if (nfq_bind_pf(h, AF_INET) < 0) 
	{
		fprintf(stderr, "Error in nfq_bind_pf()\n");
		exit(1);
	}

	//create a queue handle 
	struct nfq_q_handle* qh=nfq_create_queue(h,QUEUE_NUM, &cb, table);
	if (!qh)
	{
		fprintf(stderr, "Error in nfq_create_queue()\n");
		exit(1);
	}
	// set packet copy mode
	if (nfq_set_mode(qh, NFQNL_COPY_PACKET, 0xffff) < 0) {
		fprintf(stderr, "Could not set packet copy mode\n");
		exit(1);
	}

	//receive data and process 
	int fd=nfq_fd(h);
	int len;
	char buf[4096];
	while ((len = recv(fd, buf, sizeof(buf), 0)) && len >= 0) {
		nfq_handle_packet(h, buf, len);

	}

	//end the program and release resources
	printf("unbinding from queue 0\n");
	nfq_destroy_queue(qh);

	nfq_close(h);

	for(i=0;i<PORT_RANGE;i++)
		free(table[i]);
	free(table);

	return 0;
}

